# k3s-pi-cluster

Used to store notes/documentation/files on project to create a Raspberry Pi Cluster running k3s

## Prerequisites

* [Ansible installed](https://docs.ansible.com/ansible/latest/installation_guide/intro_installation.html) on machine to run these commands (probably locally)
* SSH access to all the nodes where K3s is going to be installed
* Sudo access to all the nodes where K3s is going to be installed

### Setting Up Nodes

* This should all be done on all of the nodes
* Modify the `/boot/lfirmware/cmdline.txt` file
  * Might be `/boot/cmdline.txt` on older distros
  * Need to add `cgroup_memory=1 cgroup_enable=memory`

## Ansible

* Copy the `example-hosts.ini` to `hosts.ini`
* Modify the `hosts.ini` file to match your use case
* From the `ansible` directory, run:
  * `ansible-playbook -i hosts.ini install_k3s.yml`

### Traefik

### Background
* [By default, Traefik](https://docs.k3s.io/networking#traefik-ingress-controller) is installed and defaults to using port 80 and 443
  * This caused problems for me as I already had an application on running on port 443
  * Added a [Helm chart config](https://doc.traefik.io/traefik-enterprise/v1.0/references/configs/helm/) and [here](https://docs.k3s.io/helm#customizing-packaged-components-with-helmchartconfig) to get it to use different ports
  * [This GitHub Issue](https://github.com/k3s-io/k3s/issues/6331) is what finally led me to the correct solution

### Deploying

From the `k8s-manifests/traefik` directory:
* `kubectl diff -f .`
* If everything looks good with the diff:
  * `kubectl apply -f .`

### Networking/Host Name Issue - Legacy iptables

I had an issue where the worker nodes were not able to talk to the master node. The command `curl -vks https://pi5:6443/cacerts` and `curl -vks https://pi5:6443/ping` were both working, however when actually trying to use it I was getting an error that the `cacerts` weren't available. When I tried to use the master node IP directly, it started working.

This is an extreme edge case, but somehow my worker node was able to lookup `pi5` from DNS maybe my router... and get the local address. When it was working I assumed it was because it was in my hosts (`/etc/hosts`) file, but it turns out it wasn't. When I added `pi5` to my hosts file it did start working with `pi5` rather than the IP. My assumption is that however it was resolving the `pi5` hostname, it wasn't able to from within k3s.

I also tried enabling the legacy IP Tables on both the master and worker nodes, and this did not seem to have an impact on anything. It seems to be commonly recommended, so perhaps it will resolve a potential issue down the line, but from what I experienced... it didn't seem necessary.

### Uninstalling
* `/usr/local/bin/k3s-agent-uninstall.sh`

### Rotating Certificates

I ran into an issue where my local kubectl commands stopped working. On checking the status of the k3s service on the master node `sudo systemctl status k3s`, there were some errors about the certificates expiring. I followed the steps in the [official K3s docs](https://docs.k3s.io/cli/certificate#rotating-client-and-server-certificates) at the time:

```
systemctl stop k3s
k3s certificate rotate
systemctl start k3s
```

I then had to update my kube config file with the updated certificates afterwards (steps follow) and this resolved the issues


## Setting Up Local Kubectl

* Copy `/etc/rancher/k3s/k3s.yaml` from the master node
* Paste as `~/.kube/config` locally
  * Modify line: `server: https://127.0.0.1:6443`
  * Change to: `server: https://pi5:6443`
    * pi5 should be whatever your master node's IP is
* `kubectl config rename-context default pis`
  * Rename the kube context to be `pis` rather than `default`

## External Secrets Operator

Used to obtain secrets from various external secret stores such as AWS Secrets Manager.

### Installation

* From the `k8s-manifests/external-secrets` directory:
* Check the output of:
  * `kubectl diff -f ns.external-secrets.yaml`
* Assuming it looks good, create the namespace:
  * `kubectl apply -f ns.external-secrets.yaml`
* Check the output of:
  * `kustomize build --enable-helm . | kubectl diff -f -`
* If it looks good:
  * `kustomize build --enable-helm . | kubectl apply -f -`

## Prometheus

Used for monitoring on the cluster

### Installation

* From the `k8s-manifests/prometheus` directory:
* Check the output of:
  * `kubectl diff -f ns.monitoring.yaml`
* Assuming it looks good, create the namespace:
  * `kubectl apply -f ns.monitoring.yaml`
* Check the output of:
  * `kustomize build --enable-helm . | kubectl diff -f -`
* If it looks good:
  * `kustomize build --enable-helm . | kubectl apply -f -`

### Accessing

We install a node port so Prometheus can be accessed by accessing any node on the server on port 32090. Something like `pi5:32090` (pi5 goes to one of the nodes in my cluster by the internal IP - can be retrieved with `kubectl get nodes -o wide`).

## Documentation
* [Medium - vvbvargas](https://medium.com/@vvbvargas/how-i-built-my-kubernetes-cluster-with-a-shared-storage-on-raspberry-pi-using-k3s-1c06b424df97)
  * Setting up NFS shared drive
* [jhqcat](https://dev.to/jhqcat/how-to-set-up-k3s-on-a-raspberry-pi-4-4343)
  * Troubleshooting networking
* [Medium - Steven Hoang](https://medium.com/@stevenhoang/step-by-step-guide-installing-k3s-on-a-raspberry-pi-4-cluster-8c12243800b9)
* [Open Source](https://opensource.com/article/20/3/kubernetes-raspberry-pi-k3s)
* [Bryan Bende](https://bryanbende.com/development/2021/05/07/k3s-raspberry-pi-initial-setup)
  * Uses [k3s-ansible](https://github.com/k3s-io/k3s-ansible) setup - prefer not to do it this way
